import { Icon } from '@iconify/react';

const Package = ({ title, price, setOpen, index }) => {
  return (
    <div className="col-md-6 col-lg-4">
      <div className={`package ${index === 2 ? 'active' : ''}`}>
        <div className="package-top">
          <h4 className='name'>{title}</h4>
          <h2 className='price'>{price}€</h2>
          <p className="mb-0 desc">Empfohlen für kleine Feiern mit bis zu 25 Gästen</p>
        </div>
        <div className="package-options">
          <div className="option">
            <div className="icon">
              <Icon icon="bi:check-lg" />
            </div>
            <p className='text'>Eigener DJ</p>
          </div>
          <div className="option">
            <div className="icon">
              <Icon icon="bi:check-lg" />
            </div>
            <p className='text'>Standard-Equipment</p>
          </div>
          <div className="option">
            <div className="icon">
              <Icon icon="bi:check-lg" />
            </div>
            <p className='text'>Anderer Grund</p>
          </div>
          <div className="option">
            <div className="icon cross">
              <Icon icon="fa-solid:times" />
            </div>
            <p className='text blur'>Unbegrenzte Zusatzstunden</p>
          </div>
          <div className="option">
            <div className="icon cross">
              <Icon icon="fa-solid:times" />
            </div>
            <p className='text blur'>Unbegrenzte Zusatzstunden</p>
          </div>
        </div>
        <button onClick={() => setOpen(true)} className='primary-btn p-btn mt-4'>Erklärung ansehen</button>
      </div>
    </div>
  )
}

export default Package;