import { useNavigate } from 'react-router-dom';

import img1 from '../../assets/images/checkout/1.png';
import img2 from '../../assets/images/checkout/2.png';
import Product from './Product';
const data = [
  { id: 1, title: 'Mikrofon', price: 38, img: img1 },
  { id: 2, title: 'Lautsprecher', price: 38, img: img2 },
]
const CheckoutProducts = () => {
  const navigate = useNavigate()
  return (
    <div className="row">
      <div className="col-lg-10 col-xxl-8">
        <div className="pirmary-table-v2">
          <div className="table-responsive">
            <table className="table table-borderless mb-0">
              <thead>
                <tr>
                  <th>Produkt name</th>
                  <th>Preis</th>
                  <th>Menge</th>
                  <th>Handlung</th>
                </tr>
              </thead>
              <tbody>
                {data.map((product, i) => <Product key={i} {...product} />)}
              </tbody>
            </table>
          </div>
          <div className="outline-btns mt-5">
            <button onClick={() => navigate('/payment')} className='primary-btn outline'>JETZT ZAHLEN</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CheckoutProducts;