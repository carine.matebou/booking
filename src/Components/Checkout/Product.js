import { Icon } from '@iconify/react';
import { useState } from 'react';

const Product = ({ img, price, title }) => {
  const [quantity, setquantity] = useState(1)
  return (
    <tr className="align-middle">
      <td>
        <div className="item">
          <div className="me-3">
            <img className="img" src={img} alt="play" />
          </div>
          <div className="item-details">
            <h4 className="title">{title}</h4>
            <span className="brand">Ein zusätzliches Mikro.</span>
          </div>
        </div>
      </td>
      <td>
        <span className="price">{price}€</span>
      </td>
      <td>
        <div className="quantity">
          <span onClick={() => {
            if (quantity > 1) {
              setquantity(quantity - 1)
            } else {
              return
            }
          }} className='icon'>
            <Icon icon="ant-design:minus-outlined" />
          </span>
          <span className="count">{quantity}</span>
          <span onClick={() => setquantity(quantity + 1)} className='icon'>
            <Icon icon="ant-design:plus-outlined" />
          </span>
        </div>
      </td>
      <td>
        <span className='icon'>
          <Icon icon="akar-icons:trash-can" width="20" height="20" color="#f40000" />
        </span>
      </td>
    </tr>
  )
}

export default Product