import React from "react";
import { NavLink, useLocation } from "react-router-dom";

const menu = [
  {
    title: "Musikwünsche",
    path: "/music-wish",
  },
  {
    title: "Persönliche Daten",
    path: "/your-data",
  },
  {
    title: "Gespräch vereinbaren",
    path: "/arrange-conversation",
  },
  {
    title: "Party",
    path: "/your-appointments",
  },
];
const PrimaryNav = () => {
  const { pathname } = useLocation();
  return (
    <ul className="primary-nav">
      {menu.map(({ title, path }) => (
        <li key={path} className={`${pathname === path ? "active" : ""}`}>
          <a>{title}</a>
        </li>
      ))}
    </ul>
  );
};

export default PrimaryNav;
