
const SectionHeader = ({ title, subTitle }) => {
  return (
    <div className="section-header">
      <h2 className='title'>{title}</h2>
      <p className='subtitle'>{subTitle}</p>
    </div>
  )
}

export default SectionHeader;