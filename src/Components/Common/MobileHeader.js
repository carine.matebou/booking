import React from 'react'
import { Dropdown } from "react-bootstrap";

import menu from '../../assets/images/menu.png';
import logo from '../../assets/images/logo-small.png';
import { Link } from 'react-router-dom';

import PrimaryNav from './PrimaryNav';

const MobileHeader = () => {
  return (
    <div className="mobile-nav">
      <div className="mobile-nav-inner">
        <div className="row g-0 align-items-center">
          <div className="col-6">
            <Dropdown>
              <Dropdown.Toggle as="button">
                <img src={menu} alt="menu" />
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <div className="menu-items">
                  <PrimaryNav />
                </div>
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <div className="col-6 text-end">
            <Link to='/'>
              <img src={logo} alt="logo" />
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default MobileHeader;