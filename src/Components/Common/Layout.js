import { Outlet } from 'react-router-dom';
import MobileHeader from './MobileHeader';
import Sidebar from "./Sidebar";

const Layout = () => {
  return (
    <>
      <Sidebar />
      <div className="main-content">
        <MobileHeader />
        <div className='body-content'>
          <div className="container-fluid">
            <Outlet />
          </div>
        </div>
      </div>
    </>
  )
}

export default Layout;