import { Link } from 'react-router-dom';
import logo from '../../assets/images/logo.png'
import PrimaryNav from './PrimaryNav';

const Sidebar = () => {
  return (
    <div className="sidebar">
      <Link to='/'>
        <img className='logo' src={logo} alt="logo" />
      </Link>
      <PrimaryNav />
    </div>
  )
}

export default Sidebar;