import { useLocation, useNavigate } from "react-router-dom";
import banner from "../../assets/images/data-1.jpg";
import { Formik, ErrorMessage } from "formik";
import { useState } from "react";
import { saveOrganization } from "../../service/form.service";
import * as Yup from "yup";
import DatePicker from "react-datepicker";
import moment from "moment";

import "react-datepicker/dist/react-datepicker.css";

const DataForm = () => {
  const navigate = useNavigate();
  const { state } = useLocation();
  const contentData = state ? state.contentData : null;
  const [startDate, setStartDate] = useState(new Date());

  const schema = Yup.object().shape({
    companyname: Yup.string().min(2, "Zu kurz!").max(50, "Zu lang!"),
    name: Yup.string()
      .min(2, "Zu kurz!")
      .max(50, "Zu lang!")
      .required("Name benötigt"),
    email: Yup.string().email("Keine Email").required("Email benötigt"),
    eventtype: Yup.string()
      .min(2, "Zu kurz!")
      .max(50, "Zu lang!")
      .required("Veranstaltungsart benötigt"),
    phonenumber: Yup.string().required("Telefonnummer benötigt"),
    budget: Yup.number()
      .typeError("Bitte geben Sie eine Zahl an")
      .required("Budget benötigt"),
    guests: Yup.number()
      .typeError("Bitte geben Sie eine Zahl an")
      .required("Gastanzahl benötigt"),
    habitacion: Yup.string().required("Wohnort benötigt"),
    residencelocation: Yup.string().required("Location benötigt"),
  });

  const submitForm = async (values) => {
    const {
      companyname,
      name,
      email,
      phonenumber,
      eventtype,
      budget,
      guests,
      residencelocation,
      habitacion,
    } = values;
    const request = {
      company: companyname,
      name: name,
      email: [email],
      phone: [phonenumber],
      title: eventtype,
      dateofevent: moment(startDate).format("YYYY-MM-DD"),
      budget: budget,
      guests: guests,
      residencelocation: residencelocation,
      habitacion: habitacion,
      content: contentData,
    };
    const res = await saveOrganization(request);
    if (res.status === 200) {
      localStorage.setItem("person_id", res?.data?.data?.person_id);
      localStorage.setItem("org_id", res?.data?.data?.org_id);
      localStorage.setItem("lead_id", res?.data?.data?.lead_id);
      localStorage.setItem("email", email);
      navigate("/arrange-conversation");
    } else {
      // error here
    }
  };
  return (
    <Formik
      initialValues={{
        companyname: "",
        name: "",
        phonenumber: "",
        email: "",
        eventtype: "",
        dateofevent: "",
        budget: "",
        guests: "",
        residencelocation: "",
        habitacion: "",
      }}
      onSubmit={submitForm}
      validationSchema={schema}
      enableReinitialize
    >
      {({
        values,
        errors,
        touched,
        handleSubmit,
        handleChange,
        handleBlur,
      }) => (
        <form onSubmit={handleSubmit}>
          <div className="row">
            <div className="col-lg-6">
              <div className="form-group">
                <label className="label" htmlFor="company">
                  Firma
                </label>
                <input
                  type="text"
                  id="company"
                  placeholder="Firma"
                  name="companyname"
                  value={values.companyname}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.name && touched.name ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="companyname"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label className="label" htmlFor="name">
                  Vorname & Nachname
                </label>
                <input
                  type="text"
                  id="name"
                  name="name"
                  placeholder="Vorname & Nachname"
                  value={values.Nachname}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.name && touched.name ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="name"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label className="label" htmlFor="phone-num">
                  Telefonnummer
                </label>
                <input
                  type="text"
                  id="phone-num"
                  placeholder="Telefonnummer"
                  name="phonenumber"
                  value={values.phonenumber}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.phonenumber && touched.phonenumber
                      ? " is-invalid"
                      : "")
                  }
                />
                <ErrorMessage
                  name="phonenumber"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label className="label" htmlFor="email">
                  Email
                </label>
                <input
                  type="text"
                  id="email"
                  placeholder="Email"
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.email && touched.email ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label className="label" htmlFor="date">
                  Eventdatum
                </label>
                <DatePicker
                  dateFormat={"dd.MM.yyyy"}
                  selected={startDate}
                  onChange={(date) => setStartDate(date)}
                  name="dateofevent"
                  className={
                    "form-control" +
                    (errors.dateofevent && touched.dateofevent
                      ? " is-invalid"
                      : "")
                  }
                />
              </div>
              <div className="form-group">
                <label className="label" htmlFor="budget">
                  Budget
                </label>
                <input
                  type="text"
                  id="budget"
                  placeholder="Budget"
                  name="budget"
                  value={values.budget}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.budget && touched.budget ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="budget"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
            </div>
            <div className="col-lg-6">
              <div className="form-group">
                <label className="label" htmlFor="ev-type">
                  Dein Wohnort
                </label>
                <input
                  type="text"
                  id="ev-type"
                  placeholder="Dein Wohnort"
                  name="habitacion"
                  value={values.habitacion}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.habitacion && touched.habitacion
                      ? " is-invalid"
                      : "")
                  }
                />
                <ErrorMessage
                  name="habitacion"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label className="label" htmlFor="ev-type">
                  Veranstaltungsart
                </label>
                <select
                  id="ev-type"
                  placeholder="Event Type"
                  name="eventtype"
                  value={values.eventtype}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.eventtype && touched.eventtype ? " is-invalid" : "")
                  }
                >
                  <option value="Hochzeit">Hochzeit</option>
                  <option value="Firmenevent">Firmenevent</option>
                  <option value="Sonstiges">Sonstiges</option>
                </select>
                <ErrorMessage
                  name="eventtype"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label className="label" htmlFor="guest">
                  Gastanzahl
                </label>
                <input
                  type="text"
                  id="guests"
                  placeholder="Gastanzahl"
                  name="guests"
                  value={values.guests}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.guests && touched.guests ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="guests"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label className="label" htmlFor="residencelocation">
                  Location
                </label>
                <input
                  type="text"
                  id="residencelocation"
                  placeholder="Location"
                  name="residencelocation"
                  value={values.residencelocation}
                  onChange={handleChange}
                  className={
                    "form-control" +
                    (errors.residencelocation && touched.residencelocation
                      ? " is-invalid"
                      : "")
                  }
                />
                <ErrorMessage
                  name="residencelocation"
                  component="div"
                  className="invalid-feedback"
                />
              </div>

              <img
                className="banner"
                style={{ maxHeight: "200px" }}
                src={banner}
                alt="banner"
              />
            </div>
          </div>
          <button type="submit" className="primary-btn mb-5 mt-4 width">
            Senden
          </button>
        </form>
      )}
    </Formik>
  );
};

export default DataForm;
