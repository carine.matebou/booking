import { useCallback, useState } from "react";
import { Calendar } from "react-big-calendar";
import moment from "moment";
import { Icon } from "@iconify/react";

const CustomToolbarDay = (toolbar, onGoBackToMonth, setSelectedDate) => {
  const goToBack = () => {
    let newDate = moment(toolbar.date).subtract(1, "d").toDate();
    setSelectedDate(newDate);
  };

  const goToNext = () => {
    let newDate = moment(toolbar.date).add(1, "d").toDate();
    setSelectedDate(newDate);
  };

  const label = () => {
    const date = moment(toolbar.date);
    return (
      <span>
        <b>{date.format("DD.MMMM")}</b>
      </span>
    );
  };

  return (
    <div className="calendar-toolbar">
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
        onClick={onGoBackToMonth}
      >
        <Icon onClick={goToBack} icon="ooui:next-rtl" />
        <h4 className="select-date ms-1">Zurück</h4>
      </div>
      <div className="right">
        <Icon onClick={goToBack} icon="ooui:next-rtl" />
        <h4 className="label">{label()}</h4>
        <Icon onClick={goToNext} icon="ooui:next-ltr" />
      </div>
    </div>
  );
};

const CustomToolbar = (toolbar, setSelectedDate) => {
  const goToBack = () => {
    let mDate = toolbar.date;
    let newDate = new Date(mDate.getFullYear(), mDate.getMonth() - 1, 1);
    setSelectedDate(newDate);
  };

  const goToNext = () => {
    let mDate = toolbar.date;
    let newDate = new Date(mDate.getFullYear(), mDate.getMonth() + 1, 1);
    setSelectedDate(newDate);
  };

  const label = () => {
    const date = moment(toolbar.date);
    return (
      <span>
        <b>{date.format("MMMM")}</b>
        <span> {date.format("YYYY")}</span>
      </span>
    );
  };

  return (
    <div className="calendar-toolbar">
      <div>
        <h4 className="select-date">Datum auswählen</h4>
      </div>
      <div className="right">
        <Icon onClick={goToBack} icon="ooui:next-rtl" />
        <h4 className="label">{label()}</h4>
        <Icon onClick={goToNext} icon="ooui:next-ltr" />
      </div>
    </div>
  );
};
const EventCalendar = ({ localizer, setConversation }) => {
  const [calendarView, setCalendarView] = useState("month");
  const [selectedDate, setSelectedDate] = useState(moment());
  const onGoBackToMonth = () => setCalendarView("month");
  const [myEvents, setEvents] = useState([]);

  const handleSelectSlot = useCallback(
    ({ start, end }) => {
      if (calendarView === "month") {
        setCalendarView("day");
        setSelectedDate(start);
        return;
      }
      const title = window.prompt("Benenne dein Event");
      if (title) {
        setEvents(() => [{ start, end, title }]);
        setConversation({ start, end, title });
      }
    },
    [setEvents, calendarView, setConversation]
  );

  const handleSelectEvent = useCallback((event) => {
    setCalendarView("day");
    setSelectedDate(event.start);
  }, []);

  return (
    <div style={{ height: "60vh", marginBottom: "40px" }}>
      <Calendar
        min={moment().set("h", 9).set("m", 0).toDate()}
        max={moment().set("h", 17).set("m", 0).toDate()}
        events={myEvents}
        localizer={localizer}
        onSelectEvent={handleSelectEvent}
        onSelectSlot={handleSelectSlot}
        view={calendarView}
        onView={(view) => setCalendarView(view)}
        onDrillDown={(date) => {
          setSelectedDate(date);
          setCalendarView("day");
        }}
        date={selectedDate}
        selectable
        timeslots={1}
        step={45}
        components={{
          toolbar:
            calendarView === "month"
              ? (toolbar) => CustomToolbar(toolbar, setSelectedDate)
              : (toolbar) => CustomToolbarDay(toolbar, onGoBackToMonth, setSelectedDate),
        }}
      />
    </div>
  );
};

export default EventCalendar;
