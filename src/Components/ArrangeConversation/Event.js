import { Icon } from "@iconify/react";

const Event = () => {
  return (
    <div className="col-lg-4 col-xxl-3">
      <div className="left">
        <div className="single-event">
          <h3 className="title">Beratung</h3>
          <p>
            <Icon className="me-2" icon="akar-icons:clock" /> ~ 45 Minuten
          </p>
          <p>Wir freuen uns darauf mit Dir/Euch den Tag zu planen.</p>
          <p>
            Wähle bitte einen Zeitslot, an dem du auf jeden Fall Zeit hast. Klicke dazu auf einen Tag und erstelle hier den Zeitslot und gib deinem Event einen Namen. <br/><br/>
            Wir schicken Dir in diesem Zeitslot dann drei konkrete Terminvorschläge für ein Online-Meeting zu.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Event;
