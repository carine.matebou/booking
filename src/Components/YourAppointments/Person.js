
const Person = ({ img, name, date, startSlot, endSlot, title }) => {
  return (
    <div className="col-sm-6 col-lg-3">
      <div className="person">
        <div className="img-box">
          <img className="img" src={img} alt="person" />
        </div>
        <div className="person-info">
          <h4 className="name">{name}</h4>
          <span className="name">{date}</span> 
          <br/>
          <span className="name">{`Zeitslot: ${startSlot} - ${endSlot}`}</span>
        </div>
      </div>
    </div>
  )
}

export default Person;