
const EventType = () => {
  return (
    <div className="col-sm-8 col-md-6 col-lg-5 col-xl-4">
      <form className="enent-type">
        <div className="form-group">
          <label className='label' htmlFor="ev-type">Veranstaltungsart</label>
          <input type="text" className="form-control" id="ev-type" placeholder='Veranstaltungsart' />
        </div>
      </form>
    </div>
  )
}

export default EventType;