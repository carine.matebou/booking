import { FaPlay } from "react-icons/fa";

const PlayList = ({ title, img }) => {
  return (
    <div className="col-sm-6 col-md-4 col-lg-3">
      <a href="#" className='single-playlist'>
        <div className="img-box">
          <img className="img" src={img} alt="play" />
        </div>
        <div className="info">
          <div className="left">
            <span className="text">Playlist</span>
            <h4 className="title">{title}</h4>
          </div>
          <div className="right">
            <div className="icon">
              <FaPlay />
            </div>
          </div>
        </div>
      </a>
    </div>
  )
}

export default PlayList;