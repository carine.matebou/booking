import playlist1 from '../../../assets/images/playlist/1.jpg';
const SongCard = () => {
  return (
    <div className="col-lg-3">
      <div className="details-left">
        <div className="img-box">
          <img className="img" src={playlist1} alt="play" />
        </div>
        <div className="info">
          <span className="text">Playlist</span>
          <h4 className="title">Kpop Hits</h4>
        </div>
        <div className="divider"></div>
        <div className="info">
          <span className="text">25 Song</span>
          <p className="desc mb-0">consectetur adipiscing elit, sed do sit eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
      </div>
    </div>
  )
}

export default SongCard