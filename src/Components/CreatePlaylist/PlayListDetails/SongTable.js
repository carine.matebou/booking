import { BsHeart, BsThreeDotsVertical } from "react-icons/bs";
import playlist5 from '../../../assets/images/playlist/5.jpg';

const SongTable = () => {
  return (
    <div className="col-lg-9">
      <div className="pirmary-table">
        <div className="table-responsive">
          <table className="table table-borderless mb-0">
            <tbody>
              {[0, 1, 2, 3, 4].map((item) => <tr key={item} className="align-middle">
                <td>
                  <div className="song">
                    <div className="me-3">
                      <span className="count">{item + 1}</span>
                      <img className="img" src={playlist5} alt="play" />
                    </div>
                    <div className="song-details">
                      <h4 className="title">Go Back to The Sky</h4>
                      <p className="brand">The Walters</p>
                    </div>
                  </div>
                </td>
                <td>
                  <span className="info">634.736.097</span>
                </td>
                <td>
                  <span className="info">04.45</span>
                </td>
                <td>
                  <BsHeart className="heart" />
                </td>
                <td>
                  <BsThreeDotsVertical className="three-dots" />
                </td>
              </tr>)}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default SongTable;