import ipod from '../../assets/images/ipod.png';

const Recommendation = () => {
  return (
    <div className="col-md-6 col-lg-4">
      <div className="recommendation">
        <img src={ipod} alt="ipod" />
        <h4 className='title'>Empfehlung 1 Sänger</h4>
      </div>
    </div>
  )
}

export default Recommendation;