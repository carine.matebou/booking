import axios from "axios";
import config from "../utils/constant";

export const saveOrganization = async (data) => {
  try {
    const res = await axios.post(`${config.BASE_URL}/api/organizations`, data);
    return { status: res?.status, data: res?.data, message: data?.message };
  } catch (err) {
    return { status: err?.response?.status };
  }
};


export const bookActivityViaCalender = async (data) => {
  try {
    const res = await axios.post(`${config.BASE_URL}/api/activity`, data);
    return { status: res?.status, data: res?.data, message: data?.message };
  } catch (err) {
    return { status: err?.response?.status };
  }
};

