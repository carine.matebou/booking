const environment = process.env.NODE_ENV;

const config = {
  BASE_URL: "",
};
if (environment === "development") {
  config.BASE_URL = "http://localhost:3001";
} else {
  config.BASE_URL = "https://backend-life-is-music.shadet.de";
}

export default config;
