import { useState } from "react";

import SectionHeader from "../Components/Common/SectionHeader";
import person1 from "../assets/images/person/1.jpg";
import Person from "../Components/YourAppointments/Person";
import { useLocation } from "react-router-dom";
import moment from "moment";
import { useEffect } from "react";

const YourAppointments = () => {
  const { state } = useLocation();
  const { start, end, title } = state.conversation;
  const [persons, setPersons] = useState([]);

  useEffect(() => {
    const data = [
      {
        id: 1,
        name: "Dawid Jankowski - Planung",
        date: moment(start).format("DD.MM.YYYY"),
        startSlot: moment(start).format("HH:mm"),
        endSlot: moment(end).format("HH:mm"),
        title: title,
        img: person1,
      },
    ];
    setPersons(data);
  }, [state]);

  return (
    <>
      <SectionHeader
        title="Deine Termine"
        subTitle={`Ich freue mich auf das Gespräch mit Dir/Euch über Dein/Euer Event ${title}.`}
      />
      <div className="form-block">
        <div className="row">
          {persons.map((person, i) => (
            <Person key={i} {...person} />
          ))}
        </div>
        <h3 className="thanks-text">Vielen Dank für DEINE/EURE Anfrage!</h3>
      </div>
    </>
  );
};

export default YourAppointments;
