import banner from '../assets/images/welcome-bg.png'
import music from '../assets/images/music.png'
import { useNavigate } from 'react-router-dom'

const Welcome = () => {
  const navigate = useNavigate();
  return (
    <section className="welcome-area">
      <div className="row g-0">
        <div className="col-lg-6 order-lg-last">
          <div className="welcome-right text-center position-relative">
            <img className='banner-img' src={banner} alt="dj" />
            <h2 className='title'>Professionelle DJ-Agentur  <br/> Life Is<span> Music </span>! </h2>
            <img className='music' src={music} alt="music" />
          </div>
        </div>
        <div className="col-lg-6 align-self-center">
          <div className="welcome-left text-center text-lg-start">
            <h1 className='title'>BUCHUNG</h1>
            <p className='text'>Wir freuen uns, dass Du uns bei Deinem großen Tag dabei haben möchtest. Wir möchten Dir gerne den Ablauf vorstellen. Die Buchung kostet Dich nur fünf Minuten Deiner Zeit, danach ist Dein Event  vollständig durchgeplant! Keine Sorge, sollte es Probleme geben stehen wir jederzeit für ein Gespräch zur Verfügung.</p>
            <button className='primary-btn' onClick={() => navigate("music-wish")}>LASS UNS ENDLICH FEIERN!</button>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Welcome