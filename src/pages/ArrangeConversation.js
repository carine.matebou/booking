import { momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "moment/locale/de";

import SectionHeader from "../Components/Common/SectionHeader";
import EventCalendar from "../Components/ArrangeConversation/EventCalendar";
import Event from "../Components/ArrangeConversation/Event";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { bookActivityViaCalender } from "../service/form.service";

const localizer = momentLocalizer(moment);

const ArrangeConversation = () => {
  const navigate = useNavigate();
  const [conversation, setConversation] = useState({
    start: null,
    end: null,
    title: null,
  });

  const bookActivityViaCalenderMethod = (start, end, title) => {
    const eventBody = {
      event_name: title,
      public_description: `Wir freuen uns auf das Gespräch mit Dir/Euch, um gemeinsam mit Dir/Euch den Tag zu planen. Den Link findest du hier: https://bit.ly/3DgYr7z. 
      Solltest du noch Fragen haben, melde Dich natürlich gerne bei uns.`,
      due_date: moment(start).utc().format("YYYY-MM-DD").toString(),
      due_time: moment(start).utc().format("HH:mm").toString(),
      duration: moment()
        .set("hours", 0)
        .set("minutes", 45)
        .format("HH:mm")
        .toString(),
      email: localStorage.getItem("email"),
      lead_id: localStorage.getItem("lead_id"),
      person_id: localStorage.getItem("person_id"),
      org_id: localStorage.getItem("org_id"),
      note: "Von API erstellt"
    };

    bookActivityViaCalender(eventBody)
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
  };

  return (
    <>
      <SectionHeader
        title="Termin mit Dawid Jankowski vereinbaren"
        subTitle="Ich freue mich darauf gemeinsam mit Dir Deinen perfekten Tag zu planen."
      />
      <div className="form-block calendar">
        <div className="row">
          <Event />
          <div className="col-lg-8 col-xxl-9">
            <EventCalendar {...{ conversation, setConversation, localizer }} />
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <button
                className="primary-btn"
                onClick={() => {
                  const start = conversation.start;
                  const end = conversation.end;
                  const title = conversation.title;
                  if (start && end) {
                    bookActivityViaCalenderMethod(start, end, title);
                    navigate("/your-appointments", {
                      state: { conversation: { start, end, title } },
                    });
                  }
                }}
              >
                WEITER
              </button>
              <button
                className="primary-btn"
                onClick={() => (window.location.href = "tel:+4924147587758")}
              >
                TERMIN PER TELEFON VEREINBAREN
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ArrangeConversation;
