import { useNavigate } from 'react-router-dom';
import React from 'react';
import { useState } from "react";
import SectionHeader from '../Components/Common/SectionHeader';
import GeneralMusicWishes from '../Components/MusicRequests/GeneralMusicWishes';
import ListMusic from '../Components/MusicRequests/ListMusic';

const MusicRequests = () => {

  const navigate = useNavigate();
  const [allRatings, setAllRatings] = useState([]);

  const handleRankingChanged = (categorie, rating) => {
    if (allRatings.some(r => r.categorie === categorie)) {
      const changedAllRatings = allRatings.map(r => {
        if (r.categorie === categorie) {
          r.rating = rating;
          return r;
        }
        return r;
      });
      setAllRatings(changedAllRatings);
    }
    else {
      setAllRatings([...allRatings, { categorie, rating }]);
    }
  }
  const [generalMusicWishes, setGeneralMusicWishes] = useState({
    firstSong: "",
    specialSongs: "",
    miscellaneous: "",
  });

  const submitData = () => {
    let contentData_generalMusicWishes = `Erstes Lied: ${generalMusicWishes.firstSong} <br> 
    NO GO's: ${generalMusicWishes.specialSongs} <br> 
    Sonstiges: ${generalMusicWishes.miscellaneous} <br> `;

    let reducedArray = allRatings.reduce((acc, curr) => `${acc}${curr.categorie} - ${curr.rating} <br>`, '')
    let allContentData = contentData_generalMusicWishes.concat(reducedArray);

    navigate("/your-data", {
      state: {
        contentData: allContentData,
      },
    });
  };

  return (
    <>
      <SectionHeader title='Musikwünsche' subTitle='Wähle nun Deine Musik, die zu deinem Anlass passt. Für Hochzeiten empfehlen wir zum Beispiel weniger fetzige Musik, aber das ist natürlich ganz Dir überlassen.' />
      <div className="form-block"  >
        <div class="flex-container">
          <div class="flex-item-left"><GeneralMusicWishes initialValue={generalMusicWishes} setInitialValue={setGeneralMusicWishes} /></div>
          <div class="flex-item-left"><ListMusic onChange={handleRankingChanged} /></div>
        </div>
        <div>
          <button className="primary-btn mb-5 mt-2 width" type="submit" onClick={submitData} > WEITER</button>
        </div>

      </div>
    </>
  )
}
export default MusicRequests;