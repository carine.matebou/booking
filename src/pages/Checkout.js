import SectionHeader from "../Components/Common/SectionHeader";
import CheckoutProducts from '../Components/Checkout/CheckoutProducts';

const Checkout = () => {
  return (
    <>
      <SectionHeader title='Zahlung und Gespräch vereinbaren' subTitle='Wenn du dir bereits sicher bist kannst Du nun bezahlen, es sollte Dir nichts mehr im Weg stehen.Vereinbare im Anschluss einen Termin mit uns, sodass wir gemeinsam mit Dir deinen Tag perfekt durchplanen können.' />
      <div className="form-block checkout-area">
        <h3 className='main-title'>DJ Premium mit Zusatzleistungen - Gesamt 1576€</h3>
        <CheckoutProducts />
        <button className='primary-btn mt-5'>TERMIN VEREINBAREN VOR ZAHLUNG</button>
      </div>
    </>
  )
}

export default Checkout;