import { useState } from 'react';
import ModalVideo from 'react-modal-video';
import { useNavigate } from 'react-router-dom';

import SectionHeader from "../Components/Common/SectionHeader";
import Package from '../Components/SelectPackage/Package';

const data = [
  { id: 1, title: 'DJ Starter', price: 1000 },
  { id: 2, title: 'DJ Premium', price: 1500 },
  { id: 3, title: 'DJ Extra', price: 2500 },
]
const SelectPackage = () => {
  const navigate = useNavigate()
  const [isOpen, setOpen] = useState(false)
  return (
    <>
      <SectionHeader title='Paket auswählen' subTitle='LNun kannst Du dein ganz persönliches Paket auswählen. Schaue Dir gerne dazu auch die kurzen Erklärungen von unserem CEO an und lasse Dich inspirieren.' />
      <div className="form-block pricing">
        <div className="row">
          {data.map((item, i) => <Package key={i} {...item} setOpen={setOpen} index={i} />)}
        </div>
        <div className="outline-btns">
          <button className='primary-btn outline'>WEITER </button>
          <button onClick={() => navigate('/additional-products')} className='primary-btn'>ZUSATZLEISTUNGEN BUCHEN (2)</button>
        </div>
      </div>
      <ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId="X-D-AS87q8I" onClose={() => setOpen(false)} />
    </>
  )
}

export default SelectPackage;