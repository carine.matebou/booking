import SectionHeader from "../Components/Common/SectionHeader";

const Payment = () => {
  return (
    <>
      <SectionHeader title='Zahlung' subTitle='DJ Premium mit Zusatzleistungen - Gesamt 1576€' />
      <div className="form-block payment-area">
        <div className="row">
          <div className="col-lg-10 col-xxl-8">
            <div className="select-payment">
              <h3 className="title">Zahlungsauswahl</h3>
              <div className="payment">
                <div className="form-check">
                  <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                  <label className="form-check-label" htmlFor="flexCheckDefault">
                    <span className="sub-title">Bezahlen Sie mit Stripe</span>
                    <p className="text">Sie werden zur Stripe-Website weitergeleitet, um Ihren Kauf sicher abzuschließen.</p>
                  </label>
                </div>
              </div>
              <div className="payment">
                <div className="form-check">
                  <input className="form-check-input" type="checkbox" value="" id="flexCheckChecked" />
                  <label className="form-check-label" htmlFor="flexCheckChecked">
                    <span className="sub-title">Zahlen Sie mit PayPal</span>
                    <p className="text">Sie werden zur Stripe-Website weitergeleitet, um Ihren Kauf sicher abzuschließen.</p>
                  </label>
                </div>
              </div>
              <div className="payment">
                <div className="form-check">
                  <input className="form-check-input" type="checkbox" value="" id="flexCheckChecked" />
                  <label className="form-check-label" htmlFor="flexCheckChecked">
                    <span className="sub-title">Kredit-/Debitkarte</span>
                    <p className="text">SSicherer Geldtransfer über Ihr Bankkonto. Wir unterstützen Mastercard, Visa, Discover und Stripe.</p>
                  </label>
                </div>
                <div className="row mt-3">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label className="label" htmlFor="card-number">Karten nummer</label>
                      <input type="text" className="form-control" id="card-number" placeholder="Geben Sie Ihre Kartennummer ein" />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label className="label" htmlFor="name">Name des Karteninhabers</label>
                      <input type="text" className="form-control" id="name" placeholder="Geben Sie Ihre Kartennummer ein" />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label className="label" htmlFor="date">Ablauf datum</label>
                      <input type="text" className="form-control" id="date" placeholder="MM/JJ" />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label className="label" htmlFor="code">CVV-Code</label>
                      <input type="text" className="form-control" id="code" placeholder="CVV-Code eingeben" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Payment;