import SectionHeader from "../Components/Common/SectionHeader";
import DataForm from "../Components/YourData/DataForm";
const YourData = () => {
  return (
    <>
      <SectionHeader
        title="Deine Daten"
        subTitle="Leider kommen wir nicht drum herum Dich um Deine Daten zu bitten, damit wir mit Dir zusammenarbeiten können. Sie werden selbstverständlich vertraulich behandelt."
      />
      <div className="form-block">
        <DataForm />
      </div>
    </>
  );
};

export default YourData;
