import { useState } from "react";

import playlist1 from '../assets/images/playlist/1.jpg';
import playlist2 from '../assets/images/playlist/2.jpg';
import playlist3 from '../assets/images/playlist/3.jpg';
import playlist4 from '../assets/images/playlist/4.jpg';
import Recommendation from "../Components/CreatePlaylist/Recommendation";
import EventType from "../Components/CreatePlaylist/EventType";
import PlayList from "../Components/CreatePlaylist/PlayList";
import SongCard from "../Components/CreatePlaylist/PlayListDetails/SongCard";
import SongTable from "../Components/CreatePlaylist/PlayListDetails/SongTable";
import { useNavigate } from "react-router-dom";

const data = [
  { id: 1, title: 'Kpop Hits', img: playlist1 },
  { id: 2, title: 'Rock', img: playlist2 },
  { id: 3, title: 'Rock', img: playlist3 },
  { id: 4, title: 'pop Hits', img: playlist4 },
]
const CreatePlaylist = () => {
  const navigate = useNavigate()
  const [playList] = useState(data)
  return (
    <>

      {/* <div className="row">
        <EventType />
      </div> */}
      <section className="recommendation-area">
        <button onClick={() => navigate('/music-wish')} className="primary-btn mb-5">ZURÜCK</button>
        <h3 className='heading'>Unsere Top 3 Empfehlungen</h3>
        <div className="row">
          {[1, 2, 3].map((item) => <Recommendation key={item} />)}
        </div>
      </section>
      <section className="palylist-card">
        <div className="row">
          {playList.map((item, i) => <PlayList key={i} {...item} />)}
        </div>
      </section>
      <section className="form-block playlist-details">
        <div className="row">
          <SongCard />
          <SongTable />
        </div>
      </section>
      <div className="btns">
        <button className="primary-btn">MUSIK ÜBERMITTELN</button>
      </div>
    </>
  )
}

export default CreatePlaylist;