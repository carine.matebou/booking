import { useNavigate } from "react-router-dom";
import SectionHeader from "../Components/Common/SectionHeader";

const data = [
  { id: 1, name: 'Lautsprecher', price: 200 },
  { id: 2, name: 'Mikrofon', price: 75 },
  { id: 3, name: 'Zusätzlicher DJ', price: 600 },
  { id: 4, name: 'Mikrofon', price: 75 },
  { id: 5, name: 'Mikrofon', price: 75 },
  { id: 6, name: 'Mikrofon', price: 75 },
]
const AdditionalProducts = () => {
  const navigate = useNavigate()
  const handleClick = (e) => {
    e.currentTarget.classList.toggle('active')
  }
  return (
    <>
      <SectionHeader title='Zusatzleistungen auswählen für DJ Starter' subTitle='Wähle Zusatzleistungen zu deinem aktuell gewählten Paket. Bitte beachte, dass manchmal ein größeres Paket mehr Sinn macht, als einzelne Zusatzleistungen.' />
      <div className="form-block product-area">
        <div className="row">
          <div className="col-lg-10 col-xxl-8">
            <div className="products">
              {data.map(({ id, name, price }) => (<div key={id} onClick={handleClick} className="product">
                <div className="left">
                  <div className="icon"></div>
                  <h4 className="name">{name}</h4>
                </div>
                <div className="right">
                  <span className="details">Details</span>
                  <h4 className="price">{price}€</h4>
                </div>
              </div>))}
            </div>
          </div>
        </div>
        <div className="outline-btns mt-5">
          <button onClick={() => navigate('/select-package')} className='primary-btn outline'>Zuruck</button>
        </div>
      </div>
    </>
  )
}

export default AdditionalProducts;